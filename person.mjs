export default class Person {
    constructor(name) {
        this.name = name;
    }

    say(phrase) {
        return `${this.name} says ${phrase}`;
    }

    getOut() {
    	console.log(`Go away, ${this.name}!!!`)
    }
}

